
// Import the functions you need from the SDKs you need
var express = require('express');
var { initializeApp } = require("firebase/app");
var { getFirestore } = require('firebase/firestore');

// Initialize Firebase
const firebaseApp = initializeApp({
  apiKey: "AIzaSyAkTwr67TenqjxF0_hqvrbjMYSCoizv1hM",
  authDomain: "mytwit-15d8d.firebaseapp.com",
  projectId: "mytwit-15d8d",
  storageBucket: "mytwit-15d8d.appspot.com",
  messagingSenderId: "301632669135",
  appId: "1:301632669135:web:eab8ef70a66ed5a32fbeba",
})

const db = getFirestore(firebaseApp);
module.exports=db;
