const express = require("express");
const router = express.Router();
// const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const config = require("config");
const User = require("../models/User");


/**
 * @swagger
 *  tags:
 *    name: User
 *    description: User api
 */

/**
 * @swagger
 * /api/users/check:
 *   post:
 *     summary: Checks if a given user is registered
 *     tags: [User]
 *     requestBody:
 *       required: true
 *       content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email: 
 *                 type: string
 *                 description: user's email address
 *                 example: dave.craig@email.com
 *              password:
 *                 type: string
 *                 description: user's password
 *                 example: password123
 *     responses:
 *       200:
 *         description: Found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  data: object
 *                  properties:
 *                    name: 
 *                      type: string
 *                    email:
 *                      type: string
 *                    address:
 *                      type: string
 *       404:
 *         description: user is not registered
 *       500:
 *         description: Server error occured
 *               
*/
router.post("/check", async (req, res) => {
  const { name, email, password } = req.body;
  try {
    console.log(email);
    let user = await User.findOne({ email, password }).select("-prikey");
    if (user) return res.send(user);
    else return res.status(200).send("Not registered"); 
  } catch (err) {
    res.status(500).send(err.message);
  }
});
//sme nice stuff
/** 
 * @swagger
 * /api/users:
 *    post:
 *       summary: Register a new user
 *       tags: [User] 
 *       requestBody:
 *          required: true
 *          content:
 *            application/json:
 *                schema:
 *                   type: object
 *                   properties:
 *                     name:
 *                       type: string
 *                       description: user's name.
 *                       example: David Craig
 *                     email: 
 *                       type: string
 *                       description: user's email address
 *                       example: dave.craig@email.com
 *                     password:
 *                       type: string
 *                       description: user's password
 *                       example: password123
 *                     address:
 *                       type: string
 *                       description: user's address
 *                       example: 0xkjoiequweiaosidf8yqwueifqeqeiqehf
 *                     encryptedKey:
 *                       type: string
 *                       description: user's encryption key
 *                       example: 
 *                     prikey:
 *                       type: string
 *                       description: user's private key
 *                       example: 0x14BexZMoP1gqvSbLZSfYigjUvfcXkroScK0d69395b06ed6c486954e971d960fe8709
 *       responses:
 *          200:
 *            description: success register
 *          500:
 *            description: server error 
 */
router.post("/", async (req, res) => {
  const { name, email, password, address, encryptedKey, prikey } = req.body;
  try {
    user = new User({
      name,
      email,
      password,
      address,
      encryptedKey,
      prikey,
      token: 0,
      blocked: false,
    });

    // const salt = await bcrypt.genSalt(10);
    // user.password = await bcrypt.hash(password, salt);

    const newuser = await user.save();
    return res.send("success register");
    // jwt.sign(
    //   payload,
    //   config.get('jwtSecret'),
    //   { expiresIn: '5 days' },
    //   (err, token) => {
    //     if (err) throw err;
    //     res.json({ token });
    //   }
    // );
  } catch (err) {
    res.status(500).send(err.message);
  }
});

module.exports = router;
