const router = require('express').Router();
const web3Utils = require('web3-utils')

const Web3 = require('web3')
const Provider = require('@truffle/hdwallet-provider')
// new Web3.providers.HttpProvider(
//     // "https://speedy-nodes-nyc.moralis.io/e08fb68a1637d6b08550068d/bsc/testnet"
//     "https://data-seed-prebsc-1-s1.binance.org:8545"
// )
// const web3 = new Web3(
//     new Web3.providers.HttpProvider(
//         "http://127.0.0.1:7545/"
//     )
// );
//0x16c10232C85aEB05CeB1a2305F59753263d21A68
const paymentContract = require("../frontend/src/truffle_abis/Payment.json")
const TOKEN_CONTRACT_ADDRESS = '0x77942689bf59dCC2Dd9C1aD75B7557681540Da96';
// const TEST_NET_TOKEN_CONTRACT_ADDRESS = "0xee31650923086260256b797658e2b8b189bd268d"

const erc20ABI = [
    {
        "constant": true,
        "inputs": [],
        "name": "name",
        "outputs": [
            {
                "name": "",
                "type": "string"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_spender",
                "type": "address"
            },
            {
                "name": "_value",
                "type": "uint256"
            }
        ],
        "name": "approve",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "totalSupply",
        "outputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_from",
                "type": "address"
            },
            {
                "name": "_to",
                "type": "address"
            },
            {
                "name": "_value",
                "type": "uint256"
            }
        ],
        "name": "transferFrom",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "decimals",
        "outputs": [
            {
                "name": "",
                "type": "uint8"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "_owner",
                "type": "address"
            }
        ],
        "name": "balanceOf",
        "outputs": [
            {
                "name": "balance",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "symbol",
        "outputs": [
            {
                "name": "",
                "type": "string"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_to",
                "type": "address"
            },
            {
                "name": "_value",
                "type": "uint256"
            }
        ],
        "name": "transfer",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "_owner",
                "type": "address"
            },
            {
                "name": "_spender",
                "type": "address"
            }
        ],
        "name": "allowance",
        "outputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "payable": true,
        "stateMutability": "payable",
        "type": "fallback"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "name": "owner",
                "type": "address"
            },
            {
                "indexed": true,
                "name": "spender",
                "type": "address"
            },
            {
                "indexed": false,
                "name": "value",
                "type": "uint256"
            }
        ],
        "name": "Approval",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "name": "from",
                "type": "address"
            },
            {
                "indexed": true,
                "name": "to",
                "type": "address"
            },
            {
                "indexed": false,
                "name": "value",
                "type": "uint256"
            }
        ],
        "name": "Transfer",
        "type": "event"
    }
];

/**
 * @swagger
 * tags: 
 *  name: Payment
 *  description: payment api
 */

/** 
 * @swagger
 *  /api/pay/send:
 *   post:
 *     summary: send payment to the tidos blockchain
 *     tags: [Payment]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               amount:
 *                 type: string
 *                 description: Amount (TDO)
 *                 example: 7.891890878799
 *               tokenContractAddress:
 *                 type: string
 *                 description: TIDOS token contract address
 *                 example: "0x77942689bf59dCC2Dd9C1aD75B7557681540Da96"
 *               from:
 *                 type: string
 *                 description: user's wallet address
 *                 example: 0xe553eab81c46c9725d456bf83a99954fd3247081
 *               priKey:
 *                 type: string
 *                 description: user's private key to sign the transaction
 *                 example: 73aa43456df2e3edb11be59ed3fafd174347d887f15605538fc6a513cce91c3d
 *     responses:
 *       201:
 *         description: Transaction created
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                     message:
 *                       type: string
 *                       description: user's name
 *                       example: transaction was created
 *                     data:
 *                       type: string
 *                       description: Transaction hash
 *                       example: 0xaskdalkjdfkadadlfjadfadf                     
 *       500:
 *         description: server error
*/
router.post('/send', async (req, res) => {
    try {
        const dt = new Date().toISOString();
        const { amount, tokenContractAddress, from, priKey } = req.body
        // const account = web3.eth.accounts.privateKeyToAccount(priKey)
        const provider = new Provider(priKey, "https://bsc-dataseed1.binance.org")
        // const provider = new Provider(priKey, "https://speedy-nodes-nyc.moralis.io/e08fb68a1637d6b08550068d/bsc/testnet")
        //https://data-seed-prebsc-1-s1.binance.org:8545
        
        const web3 = new Web3(provider);
        const networkId = await web3.eth.net.getId()

        // return res.send({contractAddress: networkId, amount, tokenContractAddress, from, priKey})
        console.log('payment address', paymentContract.networks[networkId].address, 'from', from)
        const paymentInst = new web3.eth.Contract(paymentContract.abi, paymentContract.networks[networkId].address);
        const tokenContractInst = new web3.eth.Contract(erc20ABI, TOKEN_CONTRACT_ADDRESS)

        const token = (amount * (10 ** 18)).toString()
        const newToken = web3.utils.toWei(
            (amount).toLocaleString("fullwide", { useGrouping: false }) // 1e50 or whatever allowance you want to set
        )
        // console.log('newToken', newToken)
        // console.log('token', token)
        // console.log('amount', amount)
        const approval = await tokenContractInst.methods.approve(
            paymentContract.networks[networkId].address,
            token
        )
            .send({ from })
        console.log('approval', approval)
        const allowance = await tokenContractInst.methods.allowance(from, paymentContract.networks[networkId].address).call()
        console.log('token allowance', allowance)
        const receipt = await paymentInst.methods.deposit(allowance, TOKEN_CONTRACT_ADDRESS).send({ from });

        console.log(`Transaction hash: ${receipt.transactionHash}`)
        // paymentContract.events.DepositReceived({})
        //     .on('data', (event) => console.log(event))
        //     .on('error', console.log)


        res.send({
            message: 'Transaction was mined successfully',
            data: receipt,
            address: from,
            txHash: receipt.transactionHash,
            value: token,
            from,
            tokenContractAddress,
            dt
        })
    } catch (error) {
        console.log('error handling blockchain', error.message)
        res.status(500).send({
            message: 'An error occured',
            originalMessage: error.message,
            originals: JSON.stringify(error)
        })
    }
})
// router.get()

/** 
 * @swagger
 *  /api/pay/withdraw:
 *   post:
 *     summary: withdraw payment tidos blockchain(Only TIDO owner can perform this function)
 *     tags: [Payment]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               tokenContractAddress:
 *                 type: string
 *                 description: TIDOS token contract address
 *                 example: "0x77942689bf59dCC2Dd9C1aD75B7557681540Da96"
 *               from:
 *                 type: string
 *                 description: user's wallet address
 *                 example: 0x2c48bEa60a9F76afC8e3920DAAB6B50A7A46f7bA
 *               priKey:
 *                 type: string
 *                 description: user's private key to sign the transaction
 *                 example: 1cffe4e730a351572cede31950fef19970c548c3e34394bb1c5d59fe8c6919b5
 *     responses:
 *       201:
 *         description: Transaciton created
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                     message:
 *                       type: string
 *                       description: user's name
 *                       example: transaction was created
 *                     data:
 *                       type: string
 *                       description: Transaction hash
 *                       example: 0xaskdalkjdfkadadlfjadfadf                     
 *       500:
 *         description: server error
 *         content: 
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                     message:
 *                       type: string
 *                       description: response message
 *                       example: an error occured
*/
router.post('/withdraw', async (req, res) => {
    try {
        const { tokenContractAddress, from, priKey } = req.body;

        // const networkId = await web3.eth.net.getId()
        // const paymentInst = new web3.eth.Contract(paymentContract.abi, TOKEN_CONTRACT_ADDRESS);
        // const tx = await paymentInst.methods.withdraw(tokenContractAddress)
        // const gas = await tx.estimateGas()
        // const gasPrice = await web3.eth.getGasPrice()
        // const data = tx.encodeABI()
        // const nonce = await web3.eth.getTransactionCount(from)

        // const signedTx = await web3.eth.accounts.signTransaction({
        //     to: from,
        //     data,
        //     gas,
        //     gasPrice,
        //     nonce,
        //     chainId: networkId
        // }, priKey);
        // const receipt = await web3.eth.sendSignedTransaction(signedTx.rawTransaction)
        // console.log(`Transaction hash: ${receipt.transactionHash}`)
        // paymentContract.methods.PaymentWithdrawn({}).on('data', (event) => console.log('event withdrwan',event))

        res.send({
            message: 'Transaction was mined',
            data: ''
        })
    } catch (err) {
        console.log('error handling blockchain', error.message)
        res.status(500).send({
            message: 'An error occured'
        })
    }
})

module.exports = router;