const router = require('express').Router();

const User = require("../models/User");

const Web3 = require("web3");
const web3 = new Web3(
    new Web3.providers.HttpProvider(
        "https://bsc-dataseed1.binance.org:443"
    )
);
const tokenABI = require("../frontend/src/utils/bep20abi.json");
const TOKEN_CONTRACT_ADDRESS = '0x77942689bf59dCC2Dd9C1aD75B7557681540Da96';

/**
 * @swagger
 * tags: 
 *  name: Wallet
 *  description: wallet api
 */
/**

 * @swagger
 * /api/wallet/setwallet:
 *   post:
 *     summary: set a user wallet data 
 *     tags: [Wallet]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               address:
 *                 type: string
 *                 description: The user's wallet address.
 *                 example: 0xkjoiequweiaosidf8yqwueifqeqeiqehf
 *     responses:
 *       201:
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *               type: boolean
 *               
*/

router.post('/setwallet', async (req, res) => {
    try{
    let address = req.body.address;
    var tokenInst = new web3.eth.Contract(tokenABI, TOKEN_CONTRACT_ADDRESS);
    let bal = await tokenInst.methods.balanceOf(address).call();
    let user = await User.findOne({ address });
    if (String(user.token) !== bal) {
        if (!user.blocked) {
            user.blocked = true;
            await user.save();
        }
        return res.send(true);
    }
    return res.send(false);
    }catch(err){
        res.status(500).send(err.message)
    }
});

/** 
 * @swagger
 *  /api/wallet/getaddress:
 *   post:
 *     summary: get the detail of a user's wallet address.
 *     tags: [Wallet]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The user's name.
 *                 example: David Craig
 *               email:
 *                 type: string
 *                 description: user's email address
 *                 example: dave.craig@email.com
 *               address:
 *                 type: string
 *                 description: user's wallet address
 *                 example: 0xkjoiequweiaosidf8yqwueifqeqeiqehf
 *     responses:
 *       201:
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                     name:
 *                       type: string
 *                       description: user's name
 *                       example: David Craig
 *                     email:
 *                       type: string
 *                       description: The user's email address.
 *                       example: dave.craig@email.com
 *                     address:
 *                       type: string
 *                       description: user's wallet address
 *                       example: 0xkjoiequweiaosidf8yqwueifqeqeiqehf
 *       500:
 *         description: server error
*/
router.post("/getaddress", async (req, res) => {
    try{
        let address = req.body.address;
        console.log(address);
        let user = await User.findOne({ address });
        return res.send(user);
    }catch(err){
        res.status(500).send(err.message);
    }
});

module.exports = router;