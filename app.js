var createError = require("http-errors");
var express = require("express");
const connectDB = require("./config/db");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const cors = require("cors");

const User = require("./models/User");

// swagger ui
const swaggerUi = require('swagger-ui-express')
const swaggerJsDoc = require('swagger-jsdoc')

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'TIDO Payment API',
      version: '1.0.0',
      description: 'API document for TIDO Payment'
    },
  },
  servers: [
    {
      url: 'https://tido-payment.herokuapp.com'
    }
  ],
  apis: ['./routes/*.js'], // files containing annotations as above
};

const specs = swaggerJsDoc(options);

const Web3 = require("web3");
const web3 = new Web3(
  new Web3.providers.HttpProvider(
    "https://bsc-dataseed1.binance.org:443"
  )
);
const tokenABI = require("./frontend/src/utils/bep20abi.json");
const TOKEN_CONTRACT_ADDRESS = '0x77942689bf59dCC2Dd9C1aD75B7557681540Da96';

var {
  collection,
  addDoc,
  query,
  where,
  getDocs,
  updateDoc,
} = require("firebase/firestore");
var db = require("./routes/firebase");

const usersRouter = require("./routes/users");
const walletRouter = require("./routes/wallet");
const paymentRouter = require("./routes/payment");

var app = express();

// Connect Database
connectDB();
// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');
app.use(logger("dev"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.set('views', path.join(__dirname, 'views'));
app.set("view engine","jade")
// app.use(express.static(path.join(__dirname, "public")));

//swagger ui doc 
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs))

app.use("/api/users", usersRouter);
app.use("/api/wallet", walletRouter);
app.use("/api/pay", paymentRouter);

app.get("/",  (req, res)=> {
  return res.send({data: "API IS LIVE"});
});
// app.post("/getaddress", async (req, res)=> {
//   let address = req.body.address;
//   console.log(address);
//   let user = await User.findOne({ address });
//   return res.send(user);
// });
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});
require("dotenv").config();
var PORT = process.env.PORT || "5000";
app.listen(PORT, async () => {
  console.log(`Server started on port ${PORT}`);
});
