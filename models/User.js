const mongoose=require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        unique:true
    },
    email:{
        type:String,
        required:true,
        unique:true
    },
    password:{
        type:String,
        required:true
    }, 
    address:{
        type:String,
        required:true
    },   
    encryptedKey:{
        type:mongoose.SchemaTypes.Mixed,
        required:true
    },  
    prikey:{
        type:String,
        required:true
    },
    token:{
        type:Number,
        required:true
    },
    blocked:{
        type:Boolean,
        required:true
    },
    date:{
        type:Date,
        default:Date.now
    }
});
module.exports = mongoose.model('User', UserSchema);