window.cooAbi = [
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "internalType": "address",
                "name": "to",
                "type": "address"
            },
            {
                "indexed": false,
                "internalType": "uint256[]",
                "name": "tokenIDs",
                "type": "uint256[]"
            },
            {
                "indexed": false,
                "internalType": "uint256[]",
                "name": "amounts",
                "type": "uint256[]"
            },
            {
                "indexed": false,
                "internalType": "uint256",
                "name": "claimDate",
                "type": "uint256"
            }
        ],
        "name": "CO2Claimed",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "internalType": "address",
                "name": "to",
                "type": "address"
            },
            {
                "indexed": false,
                "internalType": "uint256",
                "name": "tokenID",
                "type": "uint256"
            },
            {
                "indexed": false,
                "internalType": "uint256",
                "name": "amounts",
                "type": "uint256"
            },
            {
                "indexed": false,
                "internalType": "uint256",
                "name": "claimDate",
                "type": "uint256"
            }
        ],
        "name": "CO2Claimed2",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "internalType": "uint256",
                "name": "batchID",
                "type": "uint256"
            },
            {
                "indexed": false,
                "internalType": "uint256",
                "name": "plantDate",
                "type": "uint256"
            },
            {
                "indexed": false,
                "internalType": "uint256",
                "name": "kgOfCo2Absorbed",
                "type": "uint256"
            }
        ],
        "name": "newBatchCreated",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "internalType": "uint256",
                "name": "value",
                "type": "uint256"
            }
        ],
        "name": "returnedValue",
        "type": "event"
    },
    {
        "inputs": [],
        "name": "getnftreesContractAddress",
        "outputs": [
            {
                "internalType": "address",
                "name": "",
                "type": "address"
            }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
    },
    {
        "inputs": [
            {
                "internalType": "address",
                "name": "_co2ContractAddress",
                "type": "address"
            }
        ],
        "name": "setCO2ContractAddress",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "getCO2ContractAddress",
        "outputs": [
            {
                "internalType": "address",
                "name": "",
                "type": "address"
            }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
    },
    {
        "inputs": [
            {
                "internalType": "address",
                "name": "_poccContractAddress",
                "type": "address"
            }
        ],
        "name": "setPOCCContractAddress",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "address",
                "name": "_nftContractAddress",
                "type": "address"
            }
        ],
        "name": "setnftreesContractAddress",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "uint256",
                "name": "_kgOfCo2Absorbed",
                "type": "uint256"
            }
        ],
        "name": "createBatch",
        "outputs": [
            {
                "internalType": "uint256",
                "name": "",
                "type": "uint256"
            }
        ],
        "stateMutability": "payable",
        "type": "function",
        "payable": true
    },
    {
        "inputs": [
            {
                "internalType": "uint256",
                "name": "_batchID",
                "type": "uint256"
            },
            {
                "internalType": "uint256",
                "name": "_plantDate",
                "type": "uint256"
            },
            {
                "internalType": "uint256",
                "name": "_kgOfCo2Absorbed",
                "type": "uint256"
            }
        ],
        "name": "setBatchInfo",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function",
        "payable": true
    },
    {
        "inputs": [
            {
                "internalType": "uint256",
                "name": "_batchID",
                "type": "uint256"
            }
        ],
        "name": "getBatchById",
        "outputs": [
            {
                "components": [
                    {
                        "internalType": "uint256",
                        "name": "batchID",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "plantDate",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "kgOfCo2Absorbed",
                        "type": "uint256"
                    }
                ],
                "internalType": "struct ICoorest.Batch",
                "name": "",
                "type": "tuple"
            }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
    },
    {
        "inputs": [],
        "name": "fetchBatchs",
        "outputs": [
            {
                "components": [
                    {
                        "internalType": "uint256",
                        "name": "batchID",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "plantDate",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "kgOfCo2Absorbed",
                        "type": "uint256"
                    }
                ],
                "internalType": "struct ICoorest.Batch[]",
                "name": "",
                "type": "tuple[]"
            }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
    },
    {
        "inputs": [
            {
                "internalType": "uint256",
                "name": "_batchID",
                "type": "uint256"
            },
            {
                "internalType": "uint256[]",
                "name": "_treeIDs",
                "type": "uint256[]"
            }
        ],
        "name": "addTrees",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function",
        "payable": true
    },
    {
        "inputs": [],
        "name": "fetchNFTrees",
        "outputs": [
            {
                "components": [
                    {
                        "internalType": "uint256",
                        "name": "treeID",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "batchID",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "claimDate",
                        "type": "uint256"
                    }
                ],
                "internalType": "struct ICoorest.NFTree[]",
                "name": "",
                "type": "tuple[]"
            }
        ],
        "stateMutability": "view",
        "type": "function",
        "constant": true
    },
    {
        "inputs": [
            {
                "internalType": "uint256[]",
                "name": "_treeIDs",
                "type": "uint256[]"
            }
        ],
        "name": "removeTrees",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function",
        "payable": true
    },
    {
        "inputs": [
            {
                "internalType": "uint256",
                "name": "_treeId",
                "type": "uint256"
            }
        ],
        "name": "claimCO2ForTree",
        "outputs": [
            {
                "internalType": "uint256",
                "name": "",
                "type": "uint256"
            }
        ],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "uint256[]",
                "name": "_treeIDs",
                "type": "uint256[]"
            }
        ],
        "name": "claimCO2",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function",
        "payable": true
    },
    {
        "inputs": [
            {
                "internalType": "uint256",
                "name": "_amountCO2",
                "type": "uint256"
            }
        ],
        "name": "mintPOCC",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    }
]